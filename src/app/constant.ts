export const STATUSES: { [key: number]: string } = {
    1: "Lưu mới",
    2: "Chờ duyệt",
    3: "Đã duyệt",
    4: "Yêu cầu bổ sung",
    5: "Đã từ chối",
    6: "Chờ duyệt kết thúc",
    7: "Đã kết thúc",
    8: "Yêu cầu bổ sung kết thúc",
    9: "Từ chối kết thúc",
    0: "Đã nộp lưu",
};
export const GENDER: { [key: number]: string } = {
    0: "Nam",
    1: "Nữ"
}
export const DATAGENDER: {id:number,name:string}[] = [
    {id:0, name:"Nam"},
    {id:1, name:"Nữ"},
    {id:2, name:"Khác"},
]
export const DATATEAM: {id:number,name:string}[] = [
    {id:0, name:"Front-End"},
    {id:1, name:"Back-End"},
    {id:2, name:"Full-Stack"},
]