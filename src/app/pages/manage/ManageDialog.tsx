import React, {useState} from 'react';
import {useIntl} from 'react-intl';
import {Form, Button, Modal, Row, Col} from 'react-bootstrap';
import {Formik, Field, ErrorMessage, useFormik, useFormikContext} from 'formik';
import * as Yup from 'yup';
import {IAddnew} from './ManageModel';
import {Nav, Tabs, Tab} from 'react-bootstrap';
import EmployeeManage from './EmployeeManage';
import DiplomaEmployee from './DiplomaEmployee';
import FamilyRelation from './FamilyRelation';
import './manage.scss';
import {useDispatch} from 'react-redux';
import {ADD_NEW_EMPLOYEE} from '../../../redux/types';

const ManageDialog = (props: any) => {
  const intl = useIntl();
  const [openRegisterDialog, setOpenRegisterDialog] = useState<boolean>(false);
  const dispatch = useDispatch();
  const initialValues = {
    image: '',
    // 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAclBMVEX///9UWV1PVFmBhYdKUFS1t7hDSU5ARktFS09RVlpITlJNUlc+RElESk5MUVbGx8j4+PiipKbe39+LjpBZXmKusLLu7++8vr/P0NHY2dnl5uZiZmqTlpioqqz09PR3e35scHN8gIKPkpRfZGecnqBpbXBB8wY2AAAGRElEQVR4nO2dW3uqOhBAC4RcQEWhgHdqtf//Lx5S6/bstlYDM8zQnfXQvrq+JJNkQiZPTx6Px+PxeDwezx/meRYvtkUURcV2EWf5nPoHgbIqo1DpRMzS0JLORKJVeChX1D8MhCpupBYm+IoRWjVxRf0De1K/qG/trpbqpab+kd2pTjL5Se9DMplux9mQ60ild/XOpCpaU/9cZ/aFCh/0s4Sq2FP/ZDd2Tn5nxx31j3ZgFQhHP4sIRjN7nCb348t3mMmJ+qc/RLXp0oAfzbgZQVRdTrs14EczyiW1wD3iSQ8/y6SkVviZk+wpGART1oPxVfcWDAJdUGvcpkgABIMgYau4hRFsFbfUKt+z6z8GL0iW65tMgQkGgcqodb6yhhRsFfltNoI+E/1XTEAt9Jno0b3go6QRtdLfZHBR5oJkNRTnEDP9ZzSnhCN4H7Vw6qcr2Dh6QfHZEW9g4+gFs6EWu5BhjEKL5hJsnnGasG3EZ2q1M2hNyKYR37CasG3EN2o5Sw4/2V+RObVeyyvGXHghfaXWa8FswrYRqfXaOAO1sf+ehD7WRK4HFG6E9Es33E7KoJsu8SbDM5o6Cb7AjKSWdEFs+II33Z8xL8SG2MOQfCDm2MOwHYi0yxrk2dBCPCMuZuiGM9pQU+DO95aQ9pwGPZSSB1N0Pwup4XQAQdrpAieN+DeKUnCOP+G3bUiZ+94PYkj5xdvvb8Pfb/j7I80T/rK0XZiSGqIl9K8Qp/abAQwbUsPXAVbetEnhsvu3pI8iaD9W/P074PUAeRrib4cGyNPQCuIHU3MgNiyxEzUz6q+iUc9HLfRnpNhtmFILPm1xDy5S+s+FV7jRVDP4Lgr5hJRa7wk57U2c8D5TYe6CFYt7UIgn+QxO8S05XiMq8snwDNrKjXjzewWtEbk0IdpIZDIKLUjhlEcgPbPDSGYkrC4/HeGDjTlSS/3Fuu/l2K9MmN182kEvwDWrPmppYONpyGUqvDI3kEPRGE5Xgj4AvYDI8PphyxIu2kyoP7m8QQalOKH/9PkGNYzihHFdJRBFzoLtWOwfbriOwQv5j6W97mMEmx3TLfY9ytPYAjVjKBe17d5TFX3+9yGWolsePBXMh+CVeaHcR6NRBcOV2k1WR9e9hj4yyN87UYcPVBT8035JyHoSvEEd6Md2VEYHY/SzZHcqX77rzdQL22XoA1S7Zzm7LWlm0+cdy42SC+uyUVqEnzVNKLR6KUev98Eqft1oqXUiLInWUm5e47EFz7vs82VWx3FcZ8t8DGszj8fj8Xg8nn+SeVWt1+s8z9u/VTWmhMUP7NfLujwVzVsgpkpKOZXt6lu//5dqKoK3pjiV9ShXqftVvYg2rcf7qx03TxaNCVO71ZDyGC3q1UhE19miEVKL9MuO8MedvkmFlqJZZLw3jKvyoGWSds/rmzSRmuvrLLndyv+QsHDQtK+zlMyOL5bbcApidyGcTdMtmwz4spAapbqnlgUDyXybPJgV7UKoky1td62PU/QqSvIYU+lVJ9nzOPQxjJAniq8UHZ4C6g/BY0Lrg/NTOf0I1WFIxyoa2M9iVDRYXz1NhvezhAO9tJMJ/NvNtxAz/GOqqhmiFMZtZIPcVeMOB/SwGIU5Pc4b/IvN99ENWoJgJWgizGfCGdL2qoT/Wr0rOI9CRRx66AWNcJ3mDb9+oAsCutL3/pnHELwSPoMmriqntNIwmBBwZqxAMxRQmBmY4p5hC1pMCNRR58APHcFhApi5/41bkLkSgkTUiG4rcR8BMC+WnCb6r+jeq5ucz1LteyZ9s42gN9IwMKafIMpTVbD0e/gK4AoMPqpP6p/vPPF/elR4WXCeKK6IziVe9mPooxbVdfWG+goQJF1fFEItrQNLx/oSyFW8IOlWEWw+niZsG7HLJmOA0qRwdCpyOkCJYDi6FBtGL4gIS4fyiujPHMHS4dGkUXXSLt10gPK5sDgX463HFEktwvUK42hWbBecJ32kJ37xcH48eEwLmjOOTyiMLtA4hxrkyrIYOFarHaCaPDSO1elHN1k4TxfxCA3dvkLxhgzxht6QP97QG/LHG3pD/nhDb8gfb+gN+eMNvSF/vKE35I839Ib88YbekD/e0Bvyx9VQh2NDuxlmh2hsHMZc99zj8Xg8Ho/nn+c/deyMTxGEYaMAAAAASUVORK5CYII=',
    name: '',
    code: '',
    dateOfBirth: null,
    gender: 0,
    phone: '',
    email: '',
    address: '',
    ethnic: '',
    religion: '',
    citizenIdentificationNumber: '',
    dateOfIssuanceCard: null,
    placeOfIssueCard: '',
    team: 0,
    employeeFamilyDtos: [],
    certificatesDto: [],
  };
  const validationSchema = Yup.object().shape({
    image: Yup.string().required('Vui lòng chọn ảnh'),
    name: Yup.string()
      .matches(/^[\p{L}\s]+$/u, 'Không được nhập số và kí tự đặc biệt')
      .min(5, 'Hãy nhập đầy đủ họ và tên')
      .max(32, 'Nhập tối đa 32 kí tự')
      .required('Tên nhân viên không được để trống'),
    gender: Yup.number().oneOf([0, 1, 2], 'Chọn giới tính'),
    phone: Yup.string()
      .matches(/^\d{10}$/, 'Số điện thoại không hợp lệ')
      .max(10, 'Nhập tối đa 10 kí tự')
      .required('Số điện thoại không được để trống'),
    email: Yup.string()
      .email('Email không đúng định dạng')
      .max(64, 'Nhập tối đa 64 kí tự')
      .required('Email không được để trống'),
    code: Yup.string()
      .min(6, 'Nhập tối thiểu 6 kí tự')
      .max(32, 'Nhập tối đa 32 kí tự')
      .required('Mã nhân viên không được để trống'),
    address: Yup.string().max(200, 'Nhập tối đa 200 kí tự').required('Địa chỉ không được để trống'),
    dateOfBirth: Yup.date()
      .max(new Date(Date.now() - 567648000000), 'Yêu cầu trên 18 tuổi')
      .min(new Date(Date.now() - 1892160000000), 'Yêu cầu dưới 60 tuổi')
      .required('Vui lòng nhập ngày sinh'),
    citizenIdentificationNumber: Yup.string()
      .matches(/^(\d{9}|\d{12})$/, 'Số CCCD/CMT không hợp lệ')
      .max(12, 'Nhập tối đa 12 kí tự')
      .required('Số CCCD/CMT không được để trống'),
    team: Yup.string().required('Vui lòng chọn nhóm').nullable(),
  });
  const [activeTab, setActiveTab] = useState<string>('add');
  const [isAddTabCompleted, setIsAddTabCompleted] = useState<boolean>(false);
  const handleSubmit = (values: typeof initialValues) => {
    setIsAddTabCompleted(true);
    setActiveTab('diploma');
    console.log(values);
    dispatch({type: ADD_NEW_EMPLOYEE, values});
  };
  const handleTabSelect = (selected: any) => {
    setActiveTab(selected);
  };
  const formik = useFormik({
    initialValues,
    // validationSchema,
    onSubmit: handleSubmit,
  });

  return (
    <div className='dialog'>
      <Modal {...props} size='xl' aria-labelledby='contained-modal-title-vcenter' centered>
        <form onSubmit={formik.handleSubmit}>
          <Modal.Header closeButton>
            <Modal.Title id='contained-modal-title-vcenter'>
              {intl.formatMessage({id: 'DIALOG.ADDNEWEMPLOYEE'})}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Tabs
              defaultActiveKey='add'
              id='uncontrolled-tab-example'
              className='mb-3 tab'
              onSelect={handleTabSelect}
              activeKey={activeTab}
            >
              <Tab eventKey='add' title='Thêm mới'>
                <EmployeeManage formik={formik} />
              </Tab>

              <Tab eventKey='diploma' title='Văn bằng' disabled={!isAddTabCompleted}>
                <DiplomaEmployee formik={formik} />
              </Tab>

              <Tab eventKey='family' title='Quan hệ gia đình' disabled={!isAddTabCompleted}>
                <FamilyRelation formik={formik} />
              </Tab>
            </Tabs>
          </Modal.Body>
          <Modal.Footer className='d-flex group_btn'>
            <Button variant='primary' type='submit'>
              Submit
            </Button>
            {openRegisterDialog && <Button>Đăng ký</Button>}
            <Button variant='danger' onClick={props.onHide}>
              Close
            </Button>
          </Modal.Footer>
        </form>
      </Modal>
    </div>
  );
};
export {ManageDialog};
