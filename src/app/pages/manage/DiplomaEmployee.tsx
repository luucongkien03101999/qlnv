import React, {useState} from 'react';
import {Button, Col, Form, FormControl, FormGroup, FormLabel, Row, Table} from 'react-bootstrap';
import {DATAGENDER, DATATEAM} from '../../constant';
import * as Yup from 'yup';
import {useFormik} from 'formik';
import moment from 'moment';
type Props = {};
interface FormData {
  certificateName: '';
  field: '';
  content: '';
  issueDate: '';
}
function DiplomaEmployee(props: any) {
  const {formik} = props;
  // const dataDiploma: {}[] = [
  //   {
  //     certificateName: 'Toeic 900',
  //     issueDate: '2022-09-09',
  //     content: 'no thing',
  //     field: 'cntt',
  //   },
  //   {
  //     certificateName: 'Toeic 901',
  //     issueDate: '2022-09-09',
  //     content: 'no thing',
  //     field: 'cntt',
  //   },
  //   {
  //     certificateName: 'Toeic 902',
  //     issueDate: '2022-09-09',
  //     content: 'no thing',
  //     field: 'cntt',
  //   },
  // ];

  const [dataDiploma, setDataDiploma] = useState<{}[]>(formik.values.certificatesDto);
  const [formDataDiploma, setFormDataDiploma] = useState<FormData>({
    certificateName: '',
    field: '',
    content: '',
    issueDate: '',
  });
  const [errors, setErrors] = useState<{[key: string]: string}>({});

  const handleSubmit = (event: any) => {
    event.preventDefault();
    if (
      formDataDiploma.certificateName &&
      formDataDiploma.field &&
      formDataDiploma.content &&
      formDataDiploma.issueDate
    ) {
      dataDiploma.push(formDataDiploma);
      formik.setFieldValue(`certificatesDto`, dataDiploma);
    }
    console.log(formik.values);
    setFormDataDiploma({certificateName: '', field: '', content: '', issueDate: ''});

    const validationErrors = validateForm();
    if (Object.keys(validationErrors).length === 0) {
      // Xử lý dữ liệu form ở đây
    } else {
      // Cập nhật errors state nếu có lỗi
      setErrors(validationErrors);
    }
  };

  const handleChange = (event: any) => {
    setFormDataDiploma({
      ...formDataDiploma,
      [event.target.name]: event.target.value,
    });
  };

  const validateForm = () => {
    const errors: any = {};
    errors.certificateName = !formDataDiploma.certificateName
      ? 'Tên văng bằng không được để trống'
      : '';

    errors.field = !formDataDiploma.field ? 'Lĩnh vực không được để trống' : '';
    errors.content = !formDataDiploma.content ? 'Nội dung không được để trống' : '';
    errors.issueDate = !formDataDiploma.issueDate ? 'Ngày cấp  không được để trống' : '';
    return errors;
  };
  return (
    <div>
      <Form>
        <Form.Group controlId='formCertificateName'>
          <Form.Label>Tên văn bằng</Form.Label>
          <Form.Control
            type='text'
            name='certificateName'
            value={formDataDiploma.certificateName}
            onChange={handleChange}
            isInvalid={!!errors.certificateName}
          />
          <Form.Control.Feedback type='invalid'>{errors.certificateName}</Form.Control.Feedback>
        </Form.Group>

        <Form.Group controlId='formField'>
          <Form.Label>Lĩnh vực</Form.Label>
          <Form.Control
            type='text'
            name='field'
            value={formDataDiploma.field}
            onChange={handleChange}
            isInvalid={!!errors.field} // Đánh dấu trường lỗi
          />
          <Form.Control.Feedback type='invalid'>{errors.field}</Form.Control.Feedback>
        </Form.Group>
        <Form.Group controlId='formContent'>
          <Form.Label>Nội dung</Form.Label>
          <Form.Control
            type='text'
            name='content'
            value={formDataDiploma.content}
            onChange={handleChange}
            isInvalid={!!errors.content} // Đánh dấu trường lỗi
          />
          <Form.Control.Feedback type='invalid'>{errors.content}</Form.Control.Feedback>
        </Form.Group>
        <Form.Group controlId='formIssueDate'>
          <Form.Label>Ngày cấp</Form.Label>
          <Form.Control
            type='date'
            name='issueDate'
            value={formDataDiploma.issueDate}
            onChange={handleChange}
            isInvalid={!!errors.issueDate} // Đánh dấu trường lỗi
          />
          <Form.Control.Feedback type='invalid'>{errors.issueDate}</Form.Control.Feedback>
        </Form.Group>
        <Button className='mt-5 d-flex m-auto' onClick={handleSubmit}>
          Lưu
        </Button>
      </Form>
      {/* <form>
        <Row>
          <Col sm={6}>
            <FormGroup>
              <FormLabel>Tên văn bằng</FormLabel>
              <FormControl
                type='text'
                name='certificateName'
                value={formikDiploma.values.certificateName}
                onChange={formikDiploma.handleChange}
                onBlur={formikDiploma.handleBlur}
                isInvalid={
                  formikDiploma.touched.certificateName && formikDiploma.errors.certificateName
                }
              />
              {formikDiploma.touched.certificateName && formikDiploma.errors.certificateName && (
                <Form.Control.Feedback type='invalid'>
                  {formikDiploma.errors.certificateName}
                </Form.Control.Feedback>
              )}
            </FormGroup>
          </Col>
          <Col sm={6}>
            <FormGroup>
              <FormLabel>Lĩnh vực</FormLabel>
              <FormControl
                type='text'
                name='field'
                value={formikDiploma.values.field}
                onChange={formikDiploma.handleChange}
                onBlur={formikDiploma.handleBlur}
                isInvalid={formikDiploma.touched.field && formikDiploma.errors.field}
              />
              {formikDiploma.touched.field && formikDiploma.errors.field && (
                <Form.Control.Feedback type='invalid'>
                  {formikDiploma.errors.field}
                </Form.Control.Feedback>
              )}
            </FormGroup>
          </Col>
          <Col sm={6}>
            <FormGroup>
              <FormLabel>Nội dung</FormLabel>
              <FormControl
                type='text'
                name='content'
                value={formikDiploma.values.content}
                onChange={formikDiploma.handleChange}
                onBlur={formikDiploma.handleBlur}
                isInvalid={formikDiploma.touched.content && formikDiploma.errors.content}
              />
              {formikDiploma.touched.content && formikDiploma.errors.content && (
                <Form.Control.Feedback type='invalid'>
                  {formikDiploma.errors.content}
                </Form.Control.Feedback>
              )}
            </FormGroup>
          </Col>
          <Col sm={6}>
            <FormGroup>
              <FormLabel>Ngày cấp</FormLabel>
              <FormControl
                type='date'
                name='issueDate'
                value={formikDiploma.values.issueDate}
                onChange={formikDiploma.handleChange}
                onBlur={formikDiploma.handleBlur}
                isInvalid={formikDiploma.touched.issueDate && formikDiploma.errors.issueDate}
              />
              {formikDiploma.touched.issueDate && formikDiploma.errors.issueDate && (
                <Form.Control.Feedback type='invalid'>
                  {formikDiploma.errors.issueDate}
                </Form.Control.Feedback>
              )}
            </FormGroup>
          </Col>
        </Row>
      </form> */}
      {/* <Button className='mt-5 d-flex m-auto' onClick={() => handleSubmit(formikDiploma.values)}>
        Lưu
      </Button> */}
      <div className='diploma'>
        <Table bordered hover className='mt-5'>
          <thead>
            <tr>
              <th>Thao tác</th>
              <th>Tên văn bằng</th>
              <th>Lĩnh vực</th>
              <th>Nội dung</th>
              <th>Ngày cấp</th>
            </tr>
          </thead>
          <div className='gap'></div>
          <tbody>
            {dataDiploma.map((item: any) => (
              <tr className='table' key={item.id}>
                <Button
                  className='btn-edit'
                  // onClick={() => handleRowClick(item)}
                >
                  <i className='edit bi bi-pen'></i>
                </Button>
                <Button
                  className='btn-delete'
                  //  onClick={() => handleDeleteClick(item)}
                >
                  <i className='delete bi bi-trash3'></i>
                </Button>
                <td>{item.certificateName}</td>
                <td>{item.field}</td>
                <td>{item.content}</td>
                <td>{moment(item.issueDate).format('DD/MM/YYYY')}</td>
              </tr>
            ))}
          </tbody>
        </Table>
        {/* <PaginationCustom
          pageSize={pageSize}
          setPageSize={setPageSize}
          pageIndex={pageIndex}
          setPageIndex={setPageIndex}
          handleChange={handleChange}
          handleChangePageSize={handleChangePageSize}
        ></PaginationCustom> */}
      </div>
    </div>
  );
}

export default DiplomaEmployee;
