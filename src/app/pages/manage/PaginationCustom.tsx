import React, {useState} from 'react';
import {Dropdown, Form, FormGroup} from 'react-bootstrap';
import Pagination from 'react-bootstrap/Pagination';
import {useSelector} from 'react-redux';
import './manage.scss';
interface IProps {
  pageSize: number;
  setPageSize: any;
  pageIndex: number;
  setPageIndex: any;
  handleChange: any;
  handleChangePageSize: any;
}
const PaginationCustom: React.FC<IProps> = (props) => {
  const {pageSize, setPageSize, pageIndex, setPageIndex, handleChange, handleChangePageSize} =
    props;
  const totalPage = useSelector((state: any) => state.getTotal.totalElements);

  const totalPages = Math.ceil(totalPage / pageSize);
  const handlePageChange = (pageIndex: number) => {
    console.log(pageIndex);

    handleChange(1);
  };

  const handlePageSizeChange = (selectedSize: any) => {
    console.log(selectedSize);
    handleChangePageSize(selectedSize);
  };
  return (
    <div className='pagination'>
      <div className='rowperpage'>
        <Dropdown className='rowperpage-dropdown'>
          <Dropdown.Toggle variant='primary' id='rowPerPageDropdown' style={{padding: '6px 10px'}}>
            Rows per Page: {pageSize}
          </Dropdown.Toggle>
          <Dropdown.Menu>
            <Dropdown.Item onClick={() => handlePageSizeChange(5)}>5</Dropdown.Item>
            <Dropdown.Item onClick={() => handlePageSizeChange(10)}>10</Dropdown.Item>
            <Dropdown.Item onClick={() => handlePageSizeChange(15)}>15</Dropdown.Item>
            <Dropdown.Item onClick={() => handlePageSizeChange(30)}>30</Dropdown.Item>
            <Dropdown.Item onClick={() => handlePageSizeChange(50)}>50</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </div>
      <Pagination style={{display: 'flex'}}>
        <Pagination.First disabled={pageIndex === 1} onClick={() => handlePageChange(1)} />
        <Pagination.Prev
          onClick={() => handlePageChange(pageIndex - 1)}
          disabled={pageIndex === 1}
        />
        {Array.from({length: totalPages}).map((_, index) => (
          <Pagination.Item
            key={index + 1}
            active={index + 1 === pageIndex}
            onClick={() => handlePageChange(index + 1)}
          >
            {index + 1}
          </Pagination.Item>
        ))}
        <Pagination.Next
          onClick={() => handlePageChange(pageIndex + 1)}
          disabled={pageIndex === totalPages}
        />
        <Pagination.Last
          disabled={pageIndex === totalPages}
          onClick={() => handlePageChange(totalPages)}
        />
      </Pagination>
    </div>
  );
};

export default PaginationCustom;
