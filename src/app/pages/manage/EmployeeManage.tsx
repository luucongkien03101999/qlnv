import React from 'react';
import {Row, Col, Form, FormGroup, FormLabel, FormControl, Image} from 'react-bootstrap';
import {Field, ErrorMessage, useFormik} from 'formik';
import {DATAGENDER, DATATEAM} from '../../constant';
import {getImage, upDateImage} from '../../service/EmployeeService';
import './manage.scss';
const EmployeeManage = (props: any) => {
  const {formik} = props;
  return (
    <div className='add_employee'>
      <Row>
        <Col sm={4} className='image'>
          <Image className='img' src={formik.values.image} roundedCircle />
          <input
            id='img'
            type='file'
            onChange={(e: any) => {
              const formData = new FormData();
              formData.append('file', e.target.files[0]);
              upDateImage(formData).then((res) =>
                getImage(res.data.name).then((res) => formik.setFieldValue('image', res.config.url))
              );
            }}
          />
          <label htmlFor='img'>Chọn ảnh</label>
        </Col>
        <Col sm={8}>
          <Row>
            <Col sm={6}>
              <FormGroup>
                <FormLabel>Name</FormLabel>
                <FormControl
                  type='text'
                  name='name'
                  value={formik.values.name}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  isInvalid={formik.touched.name && formik.errors.name}
                />
                {formik.touched.name && formik.errors.name && (
                  <Form.Control.Feedback type='invalid'>{formik.errors.name}</Form.Control.Feedback>
                )}
              </FormGroup>
            </Col>
            <Col sm={6}>
              <FormGroup>
                <FormLabel>Mã nhân viên</FormLabel>
                <FormControl
                  type='text'
                  name='code'
                  value={formik.values.code}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  isInvalid={formik.touched.code && formik.errors.code}
                />
                {formik.touched.code && formik.errors.code && (
                  <Form.Control.Feedback type='invalid'>{formik.errors.code}</Form.Control.Feedback>
                )}
              </FormGroup>
            </Col>
            <Col sm={6}>
              <FormGroup>
                <FormLabel>Ngày sinh</FormLabel>
                <FormControl
                  type='date'
                  name='dateOfBirth'
                  value={formik.values.dateOfBirth}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  isInvalid={formik.touched.dateOfBirth && formik.errors.dateOfBirth}
                />
                {formik.touched.dateOfBirth && formik.errors.dateOfBirth && (
                  <Form.Control.Feedback type='invalid'>
                    {formik.errors.dateOfBirth}
                  </Form.Control.Feedback>
                )}
              </FormGroup>
            </Col>
            <Col sm={6}>
              <FormGroup>
                <FormLabel>Giới tính</FormLabel>
                <Form.Select
                  name='gender'
                  className='form-control'
                  aria-label='Default select example'
                  value={formik.values.gender}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  isInvalid={formik.touched.gender && formik.errors.gender}
                >
                  {formik.touched.gender && formik.errors.gender && (
                    <Form.Control.Feedback type='invalid'>
                      {formik.errors.gender}
                    </Form.Control.Feedback>
                  )}
                  {DATAGENDER.map((item) => (
                    <option key={item.id} value={item.id}>
                      {item.name}
                    </option>
                  ))}
                </Form.Select>
              </FormGroup>
            </Col>
            <Col sm={6}>
              <FormGroup>
                <FormLabel>Số điện thoại</FormLabel>
                <FormControl
                  type='text'
                  name='phone'
                  value={formik.values.phone}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  isInvalid={formik.touched.phone && formik.errors.phone}
                />
                {formik.touched.phone && formik.errors.phone && (
                  <Form.Control.Feedback type='invalid'>
                    {formik.errors.phone}
                  </Form.Control.Feedback>
                )}
              </FormGroup>
            </Col>
            <Col sm={6}>
              <FormGroup>
                <FormLabel>Email</FormLabel>
                <FormControl
                  type='text'
                  name='email'
                  value={formik.values.email}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  isInvalid={formik.touched.email && formik.errors.email}
                />
                {formik.touched.email && formik.errors.email && (
                  <Form.Control.Feedback type='invalid'>
                    {formik.errors.email}
                  </Form.Control.Feedback>
                )}
              </FormGroup>
            </Col>
            <Col sm={6}>
              <FormGroup>
                <FormLabel>Địa chỉ</FormLabel>
                <FormControl
                  type='text'
                  name='address'
                  value={formik.values.address}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  isInvalid={formik.touched.address && formik.errors.address}
                />
                {formik.touched.address && formik.errors.address && (
                  <Form.Control.Feedback type='invalid'>
                    {formik.errors.address}
                  </Form.Control.Feedback>
                )}
              </FormGroup>
            </Col>
            <Col sm={6}>
              <FormGroup>
                <FormLabel>Dân tộc</FormLabel>
                <FormControl
                  type='text'
                  name='ethnic'
                  value={formik.values.ethnic}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  isInvalid={formik.touched.ethnic && formik.errors.ethnic}
                />
                {formik.touched.ethnic && formik.errors.ethnic && (
                  <Form.Control.Feedback type='invalid'>
                    {formik.errors.ethnic}
                  </Form.Control.Feedback>
                )}
              </FormGroup>
            </Col>
            <Col sm={6}>
              <FormGroup>
                <FormLabel>Tôn giáo</FormLabel>
                <FormControl
                  type='text'
                  name='religion'
                  value={formik.values.religion}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  isInvalid={formik.touched.religion && formik.errors.religion}
                />
                {formik.touched.religion && formik.errors.religion && (
                  <Form.Control.Feedback type='invalid'>
                    {formik.errors.religion}
                  </Form.Control.Feedback>
                )}
              </FormGroup>
            </Col>
            <Col sm={6}>
              <FormGroup>
                <FormLabel>CCCD</FormLabel>
                <FormControl
                  type='text'
                  name='citizenIdentificationNumber'
                  value={formik.values.citizenIdentificationNumber}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  isInvalid={
                    formik.touched.citizenIdentificationNumber &&
                    formik.errors.citizenIdentificationNumber
                  }
                />
                {formik.touched.citizenIdentificationNumber &&
                  formik.errors.citizenIdentificationNumber && (
                    <Form.Control.Feedback type='invalid'>
                      {formik.errors.citizenIdentificationNumber}
                    </Form.Control.Feedback>
                  )}
              </FormGroup>
            </Col>
            <Col sm={6}>
              <FormGroup>
                <FormLabel>Ngày cấp</FormLabel>
                <FormControl
                  type='date'
                  name='dateOfIssuanceCard'
                  value={formik.values.dateOfIssuanceCard}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  isInvalid={formik.touched.dateOfIssuanceCard && formik.errors.dateOfIssuanceCard}
                />
                {formik.touched.dateOfIssuanceCard && formik.errors.dateOfIssuanceCard && (
                  <Form.Control.Feedback type='invalid'>
                    {formik.errors.dateOfIssuanceCard}
                  </Form.Control.Feedback>
                )}
              </FormGroup>
            </Col>
            <Col sm={6}>
              <FormGroup>
                <FormLabel>Nơi cấp</FormLabel>
                <FormControl
                  type='text'
                  name='placeOfIssueCard'
                  value={formik.values.placeOfIssueCard}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  isInvalid={formik.touched.placeOfIssueCard && formik.errors.placeOfIssueCard}
                />
                {formik.touched.placeOfIssueCard && formik.errors.placeOfIssueCard && (
                  <Form.Control.Feedback type='invalid'>
                    {formik.errors.placeOfIssueCard}
                  </Form.Control.Feedback>
                )}
              </FormGroup>
            </Col>
            <Col sm={6}>
              <FormGroup>
                <FormLabel>Nhóm</FormLabel>
                <Form.Select
                  name='team'
                  className='form-control'
                  aria-label='Default select example'
                  value={formik.values.team}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  isInvalid={formik.touched.team && formik.errors.team}
                >
                  {formik.touched.team && formik.errors.team && (
                    <Form.Control.Feedback type='invalid'>
                      {formik.errors.team}
                    </Form.Control.Feedback>
                  )}
                  {DATATEAM.map((item) => (
                    <option key={item.id} value={item.id}>
                      {item.name}
                    </option>
                  ))}
                </Form.Select>
                {/* <ErrorMessage name='team' component='div' className='text-danger' /> */}
              </FormGroup>
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
};

export default EmployeeManage;
