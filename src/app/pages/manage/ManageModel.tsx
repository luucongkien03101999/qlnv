export interface IAddnew {
  name?: string;
  email?: string;
  password?: string;
}
