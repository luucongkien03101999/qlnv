import React, {useEffect, useState} from 'react';
import {Button} from 'react-bootstrap';
import {ManageDialog} from './ManageDialog';
import {useIntl} from 'react-intl';
import {Table} from 'react-bootstrap';
import {useDispatch, useSelector} from 'react-redux';
import './manage.scss';
import moment from 'moment';
import {GENDER, STATUSES} from '../../constant';
import {DELETE_EMPLOYEE, GET_ALL_EMPLOYEE} from '../../../redux/types';
import PaginationCustom from './PaginationCustom';

const Manage: React.FC = () => {
  const intl = useIntl();
  const dispatch = useDispatch();
  const [pageSize, setPageSize] = useState<number>(20);
  const [pageIndex, setPageIndex] = useState<number>(1);
  useEffect(() => {
    dispatch({type: GET_ALL_EMPLOYEE, pageIndex, pageSize, listStatus});
  }, [pageSize, pageIndex]);
  const [modalShow, setModalShow] = useState<boolean>(false);
  const data = useSelector((state: any) => state.getEmployee);
  const listStatus = [1, 2, 4, 5];
  const handleChange = (pageIndex: any) => {
    setPageIndex(pageIndex);
  };
  const handleChangePageSize = (pageSize: any) => {
    setPageSize(pageSize);
  };
  const handleRowClick = (item: {}) => {
    setModalShow(true);
    if (item) {
      console.log('sua', item);
    } else {
      console.log('Theem');
    }
  };
  const handleDeleteClick = (item: any) => {
    console.log(item);

    dispatch({type: DELETE_EMPLOYEE, item});
  };

  return (
    <div className='addnew'>
      <Button className='btn_addnew' variant='primary' onClick={() => setModalShow(true)}>
        {intl.formatMessage({id: 'BUTTON.ADDNEW'})}
      </Button>
      <Table bordered hover>
        <thead>
          <tr>
            <th>Thao tác</th>
            <th>Tên</th>
            <th>Ngày sinh</th>
            <th>Giới tính</th>
            <th>Địa chỉ</th>
            <th>Số điện thoại</th>
            <th>Trạng thái</th>
          </tr>
        </thead>
        <div className='gap'></div>
        <tbody>
          {data.map((item: any) => (
            <tr className='table' key={item.id}>
              <Button className='btn-edit' onClick={() => handleRowClick(item)}>
                <i className='edit bi bi-pen'></i>
              </Button>
              <Button className='btn-delete' onClick={() => handleDeleteClick(item)}>
                <i className='delete bi bi-trash3'></i>
              </Button>
              <td>{item.name}</td>
              <td>{moment(item.dateOfBirth).format('DD/MM/YYYY')}</td>
              <td>{GENDER[item.gender]}</td>
              <td>{item.address}</td>
              <td>{item.phone}</td>
              <td>{STATUSES[item.submitProfileStatus]}</td>
            </tr>
          ))}
        </tbody>
      </Table>
      <PaginationCustom
        pageSize={pageSize}
        setPageSize={setPageSize}
        pageIndex={pageIndex}
        setPageIndex={setPageIndex}
        handleChange={handleChange}
        handleChangePageSize={handleChangePageSize}
      ></PaginationCustom>
      <ManageDialog show={modalShow} onHide={() => setModalShow(false)} />
    </div>
  );
};

export {Manage};
