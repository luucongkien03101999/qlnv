import React, {useState} from 'react';
import {Button, Col, Form, FormControl, FormGroup, FormLabel, Row, Table} from 'react-bootstrap';
import {DATAGENDER, GENDER} from '../../constant';
import PaginationCustom from './PaginationCustom';
import moment from 'moment';
import * as Yup from 'yup';
import {useFormik} from 'formik';
interface FormData {
  name: string;
  gender: number;
  dateOfBirth: string;
  relationShip: number;
  citizenIdentificationNumber: string;
  address: string;
  email: string;
  phoneNumber: string;
}
export default function FamilyRelation(props: any) {
  const {formik} = props;
  const [dataFamily, setDataFamily] = useState<{}[]>(formik.values.employeeFamilyDtos);
  const [formDataFamily, setFormDataFamily] = useState<FormData>({
    name: '',
    gender: 0,
    dateOfBirth: '',
    relationShip: 0,
    citizenIdentificationNumber: '',
    address: '',
    email: '',
    phoneNumber: '',
  });
  const [errors, setErrors] = useState<{[key: string]: string}>({});

  const handleSubmit = (event: any) => {
    event.preventDefault();
    if (
      formDataFamily.name &&
      formDataFamily.gender &&
      formDataFamily.dateOfBirth &&
      formDataFamily.relationShip &&
      formDataFamily.citizenIdentificationNumber &&
      formDataFamily.address &&
      formDataFamily.email &&
      formDataFamily.phoneNumber
    ) {
      dataFamily.push(formDataFamily);
      formik.setFieldValue(`employeeFamilyDtos`, dataFamily);
    }
    console.log(formik.values);
    // setFormDataFamily({certificateName: '', field: '', content: '', issueDate: ''});

    const validationErrors = validateForm();
    if (Object.keys(validationErrors).length === 0) {
      // Xử lý dữ liệu form ở đây
    } else {
      // Cập nhật errors state nếu có lỗi
      setErrors(validationErrors);
    }
  };

  const handleChange = (event: any) => {
    setFormDataFamily({
      ...formDataFamily,
      [event.target.name]: event.target.value,
    });
  };

  const validateForm = () => {
    const errors: any = {};
    errors.name = !formDataFamily.name ? 'Tên văng bằng không được để trống' : '';

    errors.gender = !formDataFamily.gender ? 'Lĩnh vực không được để trống' : '';
    errors.dateOfBirth = !formDataFamily.dateOfBirth ? 'Nội dung không được để trống' : '';
    errors.relationShip = !formDataFamily.relationShip ? 'Ngày cấp  không được để trống' : '';
    errors.citizenIdentificationNumber = !formDataFamily.citizenIdentificationNumber
      ? 'Ngày cấp  không được để trống'
      : '';
    errors.address = !formDataFamily.address ? 'Ngày cấp  không được để trống' : '';
    errors.email = !formDataFamily.email ? 'Ngày cấp  không được để trống' : '';
    errors.phoneNumber = !formDataFamily.phoneNumber ? 'Ngày cấp  không được để trống' : '';
    return errors;
  };
  return (
    <div>
      <Form>
        <Row>
          <Col sm={6}>
            <FormGroup controlId='formName'>
              <FormLabel>Tên</FormLabel>
              <FormControl
                type='text'
                name='name'
                value={formDataFamily.name}
                onChange={handleChange}
                isInvalid={!!errors.name}
              />

              <Form.Control.Feedback type='invalid'>{errors.name}</Form.Control.Feedback>
            </FormGroup>
          </Col>

          <Col sm={6}>
            <FormGroup controlId='formGender'>
              <FormLabel>Giới tính</FormLabel>
              <Form.Select
                name='gender'
                className='form-control'
                aria-label='Default select example'
                value={formDataFamily.gender}
                onChange={handleChange}
                isInvalid={!!errors.gender}
              >
                <Form.Control.Feedback type='invalid'>{errors.gender}</Form.Control.Feedback>

                {DATAGENDER.map((item) => (
                  <option key={item.id} value={item.id}>
                    {item.name}
                  </option>
                ))}
              </Form.Select>
            </FormGroup>
          </Col>
          <Col sm={6}>
            <FormGroup controlId='formDateOfBirth'>
              <FormLabel>Ngày sinh</FormLabel>
              <FormControl
                type='date'
                name='dateOfBirth'
                value={formDataFamily.dateOfBirth}
                onChange={handleChange}
                isInvalid={!!errors.dateOfBirth}
              />

              <Form.Control.Feedback type='invalid'>{errors.dateOfBirth}</Form.Control.Feedback>
            </FormGroup>
          </Col>
          <Col sm={6}>
            <FormGroup controlId='formPhoneNUmber'>
              <FormLabel>Số điện thoại</FormLabel>
              <FormControl
                type='text'
                name='phoneNumber'
                value={formDataFamily.phoneNumber}
                onChange={handleChange}
                isInvalid={!!errors.phoneNumber}
              />

              <Form.Control.Feedback type='invalid'>{errors.phoneNumber}</Form.Control.Feedback>
            </FormGroup>
          </Col>
          <Col sm={6}>
            <FormGroup controlId='formEmail'>
              <FormLabel>Email</FormLabel>
              <FormControl
                type='text'
                name='email'
                value={formDataFamily.email}
                onChange={handleChange}
                isInvalid={!!errors.email}
              />

              <Form.Control.Feedback type='invalid'>{errors.email}</Form.Control.Feedback>
            </FormGroup>
          </Col>
          <Col sm={6}>
            <FormGroup controlId='formCitizenIdentificationNumber'>
              <FormLabel>CCCD</FormLabel>
              <FormControl
                type='text'
                name='citizenIdentificationNumber'
                value={formDataFamily.citizenIdentificationNumber}
                onChange={handleChange}
                isInvalid={!!errors.citizenIdentificationNumber}
              />

              <Form.Control.Feedback type='invalid'>
                {errors.citizenIdentificationNumber}
              </Form.Control.Feedback>
            </FormGroup>
          </Col>
          <Col sm={6}>
            <FormGroup controlId='formRelationShip'>
              <FormLabel>Quan hệ</FormLabel>
              <Form.Select
                name='relationShip'
                className='form-control'
                aria-label='Default select example'
                value={formDataFamily.relationShip}
                onChange={handleChange}
                isInvalid={!!errors.relationShip}
              >
                <Form.Control.Feedback type='invalid'>{errors.relationShip}</Form.Control.Feedback>

                {DATAGENDER.map((item) => (
                  <option key={item.id} value={item.id}>
                    {item.name}
                  </option>
                ))}
              </Form.Select>
            </FormGroup>
          </Col>

          <Col sm={6}>
            <FormGroup controlId='formAddsress'>
              <FormLabel>Địa chỉ</FormLabel>
              <FormControl
                type='text'
                name='address'
                value={formDataFamily.address}
                onChange={handleChange}
                isInvalid={!!errors.address}
              />

              <Form.Control.Feedback type='invalid'>{errors.address}</Form.Control.Feedback>
            </FormGroup>
          </Col>
        </Row>

        <Button className='mt-5 d-flex m-auto' onClick={handleSubmit}>
          Lưu
        </Button>
      </Form>
      <div className='family'>
        <Table bordered hover className='mt-5'>
          <thead>
            <tr>
              <th>Thao tác</th>
              <th>Tên</th>
              <th>Giới tính</th>
              <th>Ngày sinh</th>
              <th>Số điện thoại</th>
              <th>Email</th>
              <th>CCCD</th>
              <th>Quan hệ</th>
              <th>Địa chỉ</th>
            </tr>
          </thead>
          <div className='gap'></div>
          <tbody>
            {dataFamily.map((item: any) => (
              <tr className='table' key={item.id}>
                <Button
                  className='btn-edit'
                  // onClick={() => handleRowClick(item)}
                >
                  <i className='edit bi bi-pen'></i>
                </Button>
                <Button
                  className='btn-delete'
                  //  onClick={() => handleDeleteClick(item)}
                >
                  <i className='delete bi bi-trash3'></i>
                </Button>
                <td>{item.name}</td>
                <td>{GENDER[item.gender]}</td>
                <td>{moment(item.dateOfBirth).format('DD/MM/YYYY')}</td>
                <td>{item.phoneNumber}</td>
                <td>{item.email}</td>
                <td>{item.citizenIdentificationNumber}</td>
                <td>{item.relationShip}</td>
                <td>{item.address}</td>
              </tr>
            ))}
          </tbody>
        </Table>
        {/* <PaginationCustom
          pageSize={pageSize}
          setPageSize={setPageSize}
          pageIndex={pageIndex}
          setPageIndex={setPageIndex}
          handleChange={handleChange}
          handleChangePageSize={handleChangePageSize}
        ></PaginationCustom> */}
      </div>
    </div>
  );
}
