import axios from 'axios';
const REACT_APP_API_ENDPOINT = 'https://em-v2.oceantech.com.vn/em';
const token = localStorage.getItem('access_token');
const headers = {
  headers: {
    Authorization: ` bearer ${token}`,
  },
};
console.log(headers);

export const getDataByIdAPI = () => {
  return axios.get(`${REACT_APP_API_ENDPOINT}/employee/391`);
};
export const getAllEmployeeAPI = (pageIndex: number, pageSize: number, listStatus: []) => {
  return axios.get(
    `${REACT_APP_API_ENDPOINT}/employee/search?pageIndex=${pageIndex}&pageSize=${pageSize}
  &keyword=hu&listStatus=${listStatus}`,
    headers
  );
};

export const getImage = (imgUrl: any) => {
  return axios.get(`${REACT_APP_API_ENDPOINT}/public/image/${imgUrl}`, headers);
};
export const addNewEmployeeAPI = (value: any) => {
  return axios.post(`${REACT_APP_API_ENDPOINT}/employee`, value, headers);
};
export const upDateImage = (file: any) => {
  return axios.post(`${REACT_APP_API_ENDPOINT}/employee/upload-image`, file, headers);
};
export const deleteEmployeeAPI = (id: any) => {
  return axios.delete(`${REACT_APP_API_ENDPOINT}/employee/${id}`, headers);
};
