import {combineReducers} from '@reduxjs/toolkit';
// import counterReducer from './reducers/counter';
// import counterSaga from './sagas/counterSaga';
import counterSaga from './EmployeeSaga';
import {all} from 'redux-saga/effects';
import getAllEmployeeSlice from '../slice/getAllEmployeeSlice';
import EmployeeSaga from './EmployeeSaga';
import getTotalSlice from '../slice/getTotalSlice';

export function* rootSaga() {
  yield all([EmployeeSaga()]);
}

const rootReducer = combineReducers({
  getEmployee: getAllEmployeeSlice,
  getTotal: getTotalSlice,
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
