import {call, takeEvery, takeLatest, put} from 'redux-saga/effects';

import {ADD_NEW_EMPLOYEE, DELETE_EMPLOYEE, GET_ALL_EMPLOYEE} from '../types';
import {
  addNewEmployeeAPI,
  deleteEmployeeAPI,
  getAllEmployeeAPI,
} from '../../app/service/EmployeeService';
import {getEmployeeSlice} from '../slice/getAllEmployeeSlice';
import {toast} from 'react-toastify';
import {getTotal} from '../slice/getTotalSlice';

export function* getAllEmployee(action: any) {
  try {
    console.log(action);
    const listEmployee: {} = yield call(
      getAllEmployeeAPI,
      action.pageIndex,
      action.pageSize,
      action.listStatus
    );
    yield put(getEmployeeSlice(listEmployee));
    yield put(getTotal(listEmployee));
    console.log('listEmployee', listEmployee);
  } catch (err) {
    console.log(err);
  }
}
export function* addNewEmployee(action: any) {
  try {
    console.log(action);

    yield call(addNewEmployeeAPI, action.values);
  } catch (err) {
    console.log(err);
  }
}
export function* deleteEmployee(action: any) {
  try {
    yield deleteEmployeeAPI(action.item.id).then((res) => {
      if (res.data.code == 200) {
        toast.success('Xóa thành công');
      } else {
        toast.error('Xóa thất bại');
      }
    });

    yield getAllEmployee(action);
  } catch (err) {
    console.log(err);
  }
}
export default function* EmployeeSaga() {
  yield takeLatest(GET_ALL_EMPLOYEE, getAllEmployee);
  yield takeLatest(ADD_NEW_EMPLOYEE, addNewEmployee);
  yield takeLatest(DELETE_EMPLOYEE, deleteEmployee);
}
