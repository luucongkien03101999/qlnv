

import { createSlice } from '@reduxjs/toolkit';

const getAllEmployeeSlice = createSlice({
    name: 'getEmployeeSlice',
    initialState: [],
    reducers: {
        getEmployeeSlice: (state, action) => {
            state = action.payload.data.data
            console.log("state",state);
            return state
        }
    },
});

export const { getEmployeeSlice } = getAllEmployeeSlice.actions;
export default getAllEmployeeSlice.reducer;
